DaughterBoard
====================

Toto je dceřiná deska pro 
[STM8 Nucleo-64 development board](https://www.st.com/en/evaluation-tools/nucleo-8s208rb.html):

* [schéma](daughterboard.pdf)
* [hlavičkový soubor](daughterboard.h)

Datasheety
-------------
* napěťová reference [TL431](datasheets/TL431.pdf)
* převodník teploty na napětí [MCP9701](datasheets/MCP9701.pdf)
* sériový LED driver [SCT2026](datasheets/SCT2026.pdf)


Foto
-------------
![](daughterboard.jpg)

Schéma
--------------

[![](daughterboard.png)](daughterboard.pdf)


Hlavičkový soubor
--------------------------------

```c

#ifndef __DAUGHTERBOARD_H__
#define __DAUGHTERBOARD_H__

/*  ----      LED      ----     */
#define LED1_PORT GPIOG
#define LED1_PIN GPIO_PIN_0
#define LED2_PORT GPIOG
#define LED2_PIN GPIO_PIN_1
#define LED3_PORT GPIOG
#define LED3_PIN GPIO_PIN_2
#define LED4_PORT GPIOG
#define LED4_PIN GPIO_PIN_3
#define LED5_PORT GPIOG
#define LED5_PIN GPIO_PIN_4
#define LED6_PORT GPIOG
#define LED6_PIN GPIO_PIN_5
#define LED7_PORT GPIOG
#define LED7_PIN GPIO_PIN_6
#define LED8_PORT GPIOG
#define LED8_PIN GPIO_PIN_7

/*  ----      Driver      ----     */
#define SDI_PORT GPIOC
#define SDI_PIN GPIO_PIN_6
#define CLK_PORT GPIOC
#define CLK_PIN GPIO_PIN_5
#define OE_PORT GPIOD
#define OE_PIN GPIO_PIN_0
#define LATCH_PORT GPIOC
#define LATCH_PIN GPIO_PIN_4

#define S1_PORT GPIOB
#define S1_PIN GPIO_PIN_2
#define S2_PORT GPIOB
#define S2_PIN GPIO_PIN_3
#define S3_PORT GPIOB
#define S3_PIN GPIO_PIN_4

// RGB LED
#define PWMB_PORT GPIOD
#define PWMB_PIN  GPIO_PIN_4
#define PWMG_PORT GPIOD
#define PWMG_PIN  GPIO_PIN_3
#define PWMR_PORT GPIOA
#define PWMR_PIN  GPIO_PIN_3

// ADC
#define VREF_PORT GPIOF
#define VREF_PIN GPIO_PIN_7
#define VTEMP_PORT GIPOF
#define VTEMP_PIN GPIO_PIN_6
#define CHANNEL_VREF  ADC2_CHANNEL_15      // 2496 mV
#define CHANNEL_VTEMP ADC2_CHANNEL_14


#endif
```
